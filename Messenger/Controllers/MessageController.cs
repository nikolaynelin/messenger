﻿using MessageService;
using MessageService.Repository;
using Messenger.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Messenger.Controllers
{
    public class MessageController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> Send(MessageVm message)
        {
            //todo:n replace with config value
            if (string.IsNullOrEmpty(message.Url))
            {
                message.Url = "http://localhost:8081/api/message/";
            }

            var response = await PostAsync(
                message.Url,
                new Message
                {
                    Body = message.Text,
                    RecipientsIds = message.RecipientsIds,
                    Subject = message.Subject
                });

            var messageResponseStr = await response.Content.ReadAsStringAsync();
            var messageResponse = JsonConvert.DeserializeObject<Response<MessageResponse>>(messageResponseStr);
            return Json(messageResponse);
        }

        private async Task<HttpResponseMessage> PostAsync(string url, object data)
        {
            var postData = JsonConvert.SerializeObject(data);
            var content = new StringContent(postData, Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                return await client.PostAsync(url, content);
            }
        }
    }
}