﻿using System.Collections.Generic;

namespace Messenger.Models
{
    public class MessageVm
    {
        public string Url { get; set; }
        public string Text { get; set; }
        public string Subject { get; set; }
        public IEnumerable<string> RecipientsIds { get; set; } = new List<string>();
    }
}