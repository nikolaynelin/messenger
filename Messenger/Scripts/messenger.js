﻿//todo:n replace with real data by json query
var dataSource = [
       { id: "8c53b436-1815-4f02-b85d-89f8c0e24aa2", firstName: 'Tim', lastName: 'Cook' },
       { id: "7c0fa3c7-ff81-4657-bfef-461cd088f795", firstName: 'Eric', lastName: 'Baker' },
       { id: "e3447392-f9df-4304-aa34-37f0e4b78813", firstName: 'Victor', lastName: 'Brown' },
       { id: "afa5bde6-b71e-45ec-a500-35b3b869ad1b", firstName: 'Lisa', lastName: 'White' },
       { id: "99552c43-3c09-4413-97b9-2e28b49b01f3", firstName: 'Oliver', lastName: 'Bull' },
       { id: "e0fbe4c9-e972-452b-af2d-56ec277559ef", firstName: 'Zade', lastName: 'Stock' },
       { id: "fc4aba59-a44e-4aa4-be28-2aa4a79b431e", firstName: 'David', lastName: 'Reed' },
       { id: "b2e41313-1e1f-4331-b125-fa4d2e1aa954", firstName: 'George', lastName: 'Hand' },
       { id: "62077a12-77e6-44f6-9cc8-ad57b63dabcb", firstName: 'Tony', lastName: 'Well' },
       { id: "0e6436b5-572d-4d03-9e81-ae80a9083709", firstName: 'Bruce', lastName: 'Wayne' },
];
$(function () {
    $('#recipients').magicsearch({
        dataSource: dataSource,
        fields: ['firstName', 'lastName'],
        id: 'id',
        format: '%firstName% %lastName%',
        multiple: true,
        multiField: 'firstName',
        noResult: 'No results',
        focusShow: true
    });

    $(".send-btn").click(function (e) {
        var recipients = [];
        $(".multi-item").each(function (i, el) {
            recipients.push($(el).data("id"));
        });

        if (!recipients[0]) {
            alert("Fill recipients field!");
            return;
        }

        if (!$(".message-text").val()) {
            alert("Fill message field!");
            return;
        }

        $.post("/message/send", {
            Url: $(".service-url").val(),
            Text: $(".message-text").val(),
            Subject: $(".subject").val(),
            RecipientsIds: recipients
        }, function (r) {
            $(".message-text").val("");
            $(".subject").val("");

            //todo:n replace with toastr
            $(".resp-text").text("Success! Message id: " + r.Data.Id);
            $(".message-text").focus();
        }).error(function (r) {
            //todo:n replace with toastr
            $(".resp-text").text("Error " + r.status + "!" + " " + r.statusText);
        });
    })
});