﻿using MessageService.Repository;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace MessageService.Api
{
    public class MessageController : ApiController
    {
        //todo:n replace with DI
        private readonly IMessageRepository _messageRepo = new MessageRepository();
        private readonly INotificationService _notificationService = new NotificationService();

        public System.Web.Http.Results.JsonResult<Response<MessageResponse>> Post(Message message)
        {
            var id = _messageRepo.Save(message);

            NotifyAllRecipientsAsync(message);

            return Json(new Response<MessageResponse> { Data = new MessageResponse { Id = id } });
        }

        private async Task NotifyAllRecipientsAsync(Message message)
        {
            await Task.Run(() =>
            {
                var sentCount = 0;
                message.RecipientsIds.ToList().ForEach(recipient =>
                {
                    var success = _notificationService.Notify(
                        new Notification
                        {
                            Body = message.Body,
                            Recipient = recipient
                        });
                    if (success)
                    {
                        sentCount++;
                    }
                });
                if (sentCount < message.RecipientsIds.Count())
                {
                    message.IsSent = false;
                    _messageRepo.Save(message);
                }
            });
        }
    }
}
