﻿using MessageService.Repository;
using System;

namespace MessageService
{
    public interface INotificationService
    {
        bool Notify(Notification notification);
    }
    public class NotificationService : INotificationService
    {
        private static Random _rand = new Random();
        public bool Notify(Notification notification)
        {
            //return false for 10% of notifications
            var rand = _rand.Next(1, 100);
            return rand > 90 ? false : true;
        }
    }
}