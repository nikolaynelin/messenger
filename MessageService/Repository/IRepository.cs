﻿namespace MessageService.Repository
{
    public interface IRepository<T> where T : EntityBase
    {
        string Save(T entity);
    }
}