﻿namespace MessageService.Repository
{
    public class Notification
    {
        public string Body { get; set; }
        public string Recipient { get; set; }
    }
}