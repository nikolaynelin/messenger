﻿using System.Collections.Generic;

namespace MessageService.Repository
{
    public class Message : EntityBase
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool? IsSent { get; set; }
        public IEnumerable<string> RecipientsIds { get; set; } = new List<string>();
    }
}