﻿namespace MessageService.Repository
{
    public interface IMessageRepository : IRepository<Message>
    {
    }

    public class MessageRepository : RepositoryBase<Message>, IMessageRepository
    {
    }
}