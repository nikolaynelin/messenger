﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MessageService.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : EntityBase
    {
        private static List<T> _collection = new List<T>();
        public string Save(T entity)
        {
            if (entity.Id != Guid.Empty)
            {
                var old = _collection.First(x => x.Id == entity.Id);
                _collection.Remove(old);
                _collection.Add(entity);
            }
            else
            {
                entity.Id = Guid.NewGuid();
                _collection.Add(entity);
            }
            return entity.Id.ToString();
        }
    }
}