﻿using System;

namespace MessageService.Repository
{
    public abstract class EntityBase : IEntity
    {
        public Guid Id { get; set; }
    }
}